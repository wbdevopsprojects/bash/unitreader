#!/bin/bash

set -euxo pipefail

INPUT="/etc/script/data.json"
OUTPUT="/tmp/result"

# Валидировать json файл 
jsonlint $INPUT

# Создаем папку если не существует 
mkdir -p $OUTPUT

sudo rm -rf $OUTPUT/*

# Извлекаем данные из json 
jq -c '.[]' "$INPUT" | while read i; do

postal_code=$(echo "$i" | jq '.postal_code' -r) 
id=$(echo "$i" | jq '.id' -r)  
name=$(echo "$i"| jq '.first_name' -r)
address=$(echo "$i" | jq '.address' -r)  
currency=$(echo "$i"| jq '.currency' -r)

# if [[ -n $id ]]
# then 

# создаем yaml файл
cat > "$OUTPUT"/"${postal_code}_$id".yaml <<EOT
person_${id}:
name: ${name}
address: ${address}
curr: ${currency}   
EOT

# fi
   
done;



